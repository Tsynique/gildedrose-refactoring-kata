<?php

namespace App;

use PHPUnit\Framework\TestCase;

class ItemUpdaterTest extends TestCase {

    /**
     * @dataProvider dataForUpdateQuality
     * @uses \App\Item::__construct
     * @uses \App\GildedRose::__construct
     * @uses \App\GildedRose::updateQuality
     */
    public function testUpdateQuality(Item $testItem, Item $expectedItem): void {
        $gildedRose = new GildedRose([$testItem]);
        $gildedRose->updateQuality();
        $this->assertEquals($expectedItem, $testItem);
    }

    public function dataForUpdateQuality(): array {
        return [
            'At the end of each day our system lowers both values for every item' => [
                'testItem' => new Item('foo', 2, 10),
                'expectedItem' => new Item('foo', 1, 9),
            ],
            'Once the sell by date has passed, Quality degrades twice as fast' => [
                'testItem' => new Item('old item', 0, 10),
                'expectedItem' => new Item('old item', -1, 8),
            ],
            'The Quality of an item is never negative' => [
                'testItem' => new Item('low quality', 2, 0),
                'expectedItem' => new Item('low quality', 1, 0),
            ],
            'The Quality of an item is never negative. Even after sell_in is negative' => [
                'testItem' => new Item('old', 0, 1),
                'expectedItem' => new Item('old', -1, 0),
            ],
            'The Quality of an item is never negative. Even after sell_in is negative and zero quality' => [
                'testItem' => new Item('low quality and old', 0, 0),
                'expectedItem' => new Item('low quality and old', -1, 0),
            ],
            '"Aged Brie" actually increases in Quality the older it gets' => [
                'testItem' => new Item('Aged Brie', 2, 10),
                'expectedItem' => new Item('Aged Brie', 1, 11),
            ],
            '"Aged Brie" actually increases in Quality the older it gets. Even after sell_in is negative' => [
                'testItem' => new Item('Aged Brie', 0, 10),
                'expectedItem' => new Item('Aged Brie', -1, 12),
            ],
            'an item can never have its Quality increase above 50' => [
                'testItem' => new Item('Aged Brie', 2, 50),
                'expectedItem' => new Item('Aged Brie', 1, 50),
            ],
            '"Sulfuras", being a legendary item, never has to be sold or decreases in Quality' => [
                'testItem' => new Item('Sulfuras, Hand of Ragnaros', 10, 25),
                'expectedItem' => new Item('Sulfuras, Hand of Ragnaros', 10, 25),
            ],
            '"Sulfuras", being a legendary item, never has to be sold or decreases in Quality but stays above 50' => [
                'testItem' => new Item('Sulfuras, Hand of Ragnaros', 10, 80),
                'expectedItem' => new Item('Sulfuras, Hand of Ragnaros', 10, 80),
            ],
            '"Backstage passes", increases in Quality as its SellIn value approaches, but not above 50' => [
                'testItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 20, 50),
                'expectedItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 19, 50),
            ],
            '"Backstage passes", increases in Quality as its SellIn value approaches' => [
                'testItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 20, 10),
                'expectedItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 19, 11),
            ],
            '"Backstage passes", increases in Quality as its SellIn value approaches. By 2 when 10 days or less' => [
                'testItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 10, 10),
                'expectedItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 9, 12),
            ],
            '"Backstage passes", increases in Quality as its SellIn value approaches. By 3 when 5 days or less' => [
                'testItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 5, 10),
                'expectedItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 4, 13),
            ],
            '"Backstage passes", increases in Quality as its SellIn value approaches, 0 after concert' => [
                'testItem' => new Item('Backstage passes to a TAFKAL80ETC concert', 0, 10),
                'expectedItem' => new Item('Backstage passes to a TAFKAL80ETC concert', -1, 0),
            ],
            '"Conjured" items degrade in Quality twice as fast as normal items' => [
                'testItem' => new Item('Conjured', 2, 10),
                'expectedItem' => new Item('Conjured', 1, 8),
            ],
        ];
    }
}
