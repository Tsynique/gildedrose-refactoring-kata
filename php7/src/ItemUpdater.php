<?php

namespace App;

class ItemUpdater {
    const MAX_QUALITY = 50;

    private Item $item;

    public function __construct(Item $item) {
        $this->item = $item;
    }

    public function update(): void {
        if ($this->itemIsAgedBrie()) {
            $this->updateAgedBrie();

        } elseif ($this->itemIsConjured()) {
            $this->updateConjured();

        } elseif ($this->itemIsSulfuras()) {
            $this->updateSulfuras();

        } elseif ($this->itemIsBackstagePasses()) {
            $this->updateBackstagePasses();

        } else {
            $this->updateSimpleItem();
        }
    }

    /**
     * At the end of each day our system lowers both values for every item
     * Once the sell by date has passed, Quality degrades twice as fast
     */
    private function updateSimpleItem() {
        $this->decreaseSellIn();
        $this->decreaseQuality();
        if ($this->sellInDateHasPassed()) {
            $this->decreaseQuality();
        }
    }

    /**
     * "Aged Brie" actually increases in Quality the older it gets
     */
    private function updateAgedBrie(): void {
        $this->decreaseSellIn();
        $this->increaseQuality();
        if ($this->sellInDateHasPassed()) {
            $this->increaseQuality();
        }
    }

    /**
     * "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
     * If the requirement ever changes, add some code here
     */
    private function updateSulfuras(): void {
        
    }

    /**
     * "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
     * Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but
     * Quality drops to 0 after the concert
     */
    private function updateBackstagePasses(): void {
        $this->decreaseSellIn();
        if ($this->hasItemDatePassed()) {
            $this->decreaseQuality($this->item->quality);
        } elseif ($this->item->sell_in <= 5) {
            $this->increaseQuality(3);
        } elseif ($this->item->sell_in <= 10) {
            $this->increaseQuality(2);
        } else {
            $this->increaseQuality();
        }
    }

    /**
     * "Conjured" items degrade in Quality twice as fast as normal items
     */
    private function updateConjured(): void {
        $this->decreaseSellIn();
        $this->decreaseQuality(2);
    }

    private function itemIsConjured(): bool {
        return ($this->item->name === 'Conjured');
    }

    private function hasItemDatePassed(): bool {
        return ($this->item->sell_in < 0);
    }

    private function itemIsAgedBrie(): bool {
        return ($this->item->name === 'Aged Brie');
    }

    private function itemIsSulfuras(): bool {
        return ($this->item->name === 'Sulfuras, Hand of Ragnaros');
    }

    private function itemIsBackstagePasses(): bool {
        return ($this->item->name === 'Backstage passes to a TAFKAL80ETC concert');
    }

    /**
     * an item can never have its Quality increase above 50
     */
    private function increaseQuality(int $byAmount = 1): void {
        if ($this->item->quality < self::MAX_QUALITY) {
            $this->item->quality += $byAmount;
        }
    }

    private function decreaseQuality(int $byAmount = 1): void {
        if ($this->item->quality > 0) {
            $this->item->quality -= $byAmount;
        }
    }

    private function decreaseSellIn(int $byAmount = 1): void {
        $this->item->sell_in -= $byAmount;
    }

    private function sellInDateHasPassed(): bool {
        return ($this->item->sell_in < 0);
    }
}
